// Libraries
import React from 'react';
import { isMobile, browserName } from "react-device-detect";
import { Logo } from '../Logo';
import { Header } from '../Header';

// Style
import './PrivacyPolicy.scss';


function PrivacyPolicy() {

  /**
   *
   */
  return (
    <div>
      <Header />
      <Logo />
    <div id="sec2" className="second-section">
      <div className="container text-center">
        <h1 className="text-center pt-5 pb-4 second-section--title">The Revolution Token Privacy and Cookie Policy</h1>
        <p>The Revolution Token (“TRT”) is committed to protecting and respecting consumers privacy. This Privacy and Cookie Policy (“Policy”) govern your access to and use of this Website, <a href="/">www.therevolutiontoken.com</a> (the “Website”), and associated content, software, and mobile applications (collectively, the “Service”). This Privacy Policy also includes our Terms of Use.</p>
        <p>This Policy explains when and why we collect personal information about people who visit our website or use our Services and how we use the personal information, the conditions under which we may disclose your personal information to others, and how we keep personal information secure. This Policy also explains how we use cookies and similar technology on our website and in connection with our Services.</p>
        <p>We may change this Policy from time to time so please check this page occasionally to ensure that you are happy with any changes. By using our website or our Services, you are agreeing to be bound by this Policy.</p>
        <h1 className="text-center pt-5 pb-4 second-section--title">What Information Do We Collect?</h1>
        <p>The Revolution Token my collect (a) the e-mail addresses of those who communicate with us via e-mail; (b) aggregate information concerning what pages users access or visit; (c) information volunteered by the user (such as survey information and/or site registrations); (d) financial information provided by the user for user syncing and information purposes only; and (e) information related to your use of the website and/or the mobile application, including IP address, geographic location, and date and time of your request.</p>
        <h1 className="text-center pt-5 pb-4 second-section--title">How Do We Use the Information?</h1>
        <p>The Revolution Token uses collected information for the following purposes:</p>
        <ul>
          <li>To fulfill a contract or take steps linked to a contract such as processing your registration on our website or sending you information about changes to our terms or policies.</li>
          <li>Where it is necessary for purposes which are in The Revolution Token or third parties’ legitimate interests such as (a) to provide the information or content you have requested; (b) to contact you about our programs, products, features or services; (c) for internal business purposes such as identification and authentication or customer service, portfolio tracking and user preference syncing between devices; (d) to ensure the security of our Website, by trying to prevent unauthorized or malicious activities; (e) to enforce compliance with our Terms of Use and other policies; (f) to help other organizations (such as copyright owners) to enforce their rights; and (g) to tailor content, advertisements, and offers for you or for other purposes disclosed at the time of collection. If you do not wish to receive marketing information about our programs, products, features or services, you may send an email to info@therevolutiontoken.com.</li>
          <li>Where you give us consent, such as (a) where you ask us to send marketing information to you via a medium where we need your consent, including alerts via mobile push notifications; (b) where you give us consent to place cookies and to use similar technologies; and (c) on other occasions where we ask you for consent, for a purpose which we explain at that time</li>
          <li>Where we are legally required to do so. We may also provide access to your personally identifiable information when legally required to do so, to cooperate with police investigations or other legal proceedings, to protect against misuse or unauthorized use of our Website, to limit our legal liability and protect our rights, or to protect the rights, property or safety of visitors of the Website or the public. In those instances, the information is provided only for that purpose.</li>
        </ul>
        <h1 className="text-center pt-5 pb-4 second-section--title">How Do We Share Your Information?</h1>
        <p>We do not share or sell your personal data to other organizations for commercial purposes, except to provide products or services you’ve requested, when we have your permission, or under the following circumstances:</p>
        <ul>
        <li>It is necessary to share information in order to investigate, prevent, or take action regarding illegal activities, suspected fraud, situations involving potential threats to the physical safety of any person, violations of Terms of Service, or as otherwise required by law.</li>
        <li>We transfer information about you if The Revolution Token is acquired by or merged with another company. In this event, The Revolution Token will notify you before information about you is transferred and becomes subject to a different Policy.</li>
        <li>We provide such information to trusted businesses or persons for the sole purpose of processing personally identifying information on our behalf. When this is done, it is subject to agreements that oblige those parties to process such information only on our instructions and in compliance with this Policy and appropriate confidentiality and security measures.</li>
        <li>We provide such information to third parties who have entered into non-disclosure agreements with us.</li>
        <li>We provide such information to a company controlled by, or under common control with, The Revolution Token for any purpose permitted by this Policy.</li>
        <li>We may aggregate, anonymize, and publish data for statistical and research purposes only. For example, we may compile and share information related to the popularity of certain products tracked by users. In any such instance, the information will not be able to be traced back to any individual.</li>
        </ul>
        <h1 className="text-center pt-5 pb-4 second-section--title">Cookies and Web Beacons</h1>
        <p>A cookie is a small amount of data, which often includes an anonymous unique identifier, which is sent to your browser from a Website’s computers and stored on your computer’s hard drive. Cookies are required to use some services. The Revolution Token and its ad management partners (“Ad Partners”) use cookies to record current session information.</p>
        <p>Our Ad Partners may also from time to time use web beacons (also known as Internet tags, pixel tags, and clear GIFs). These web beacons are provided by our Ad Partners and allow Ad Partners to obtain information such as the IP address of the computer that downloaded the page on which the beacon appears, the URL of the page on which the beacon appears, the time the page containing the beacon was viewed, the type of browser used to view the page, and the information in cookies set by the Ad Partners. Web beacons enable our Ad Partners to recognize a unique cookie on your web browser, which in turn enables us to learn which advertisements bring users to our Website.</p>
        <p>With both cookies and web beacon technology, the information that we collect, and share is anonymous and not personally identifiable. It does not contain your name, address, geographic location, telephone number, or e-mail address.<br/>You can opt-out of Google Analytics data collection with the Google Analytics Opt-out Browser Add-on.</p>
        <h1 className="text-center pt-5 pb-4 second-section--title">Data Storage</h1>
        <p>The Revolution Token uses third-party vendors and hosting partners to provide the necessary hardware, software, networking, storage, and related technology required to run The Revolution Token and the related website and mobile application. The Revolution Token owns the code, databases, and all rights to The Revolution Token website, Launchpad, Workspace, The Revolverse and all applicable TRT services.</p>
        <h1 className="text-center pt-5 pb-4 second-section--title">Security</h1>

        <p>We take precautions to ensure the security of your personal information. However, we cannot guarantee that hackers or unauthorized personnel may gain access to your personal information despite our efforts. You should note that in using The Revolution Token service, your information will travel over the Internet and through third-party infrastructures and mobile networks, which are not under our control.</p>
        <p>We cannot protect, nor does this Policy apply to, any information that you transmit to other users. You should never transmit personal or identifying information to other users.</p>
        <h1 className="text-center pt-5 pb-4 second-section--title">Retention of Your Personal Information</h1>
        <p>We retain information if it is necessary to provide the Services requested by you and others, subject to any legal obligations to further retain such information.</p>
        <p>Information associated with your account will generally be kept until it is no longer necessary to provide the Services or until you ask us to delete it or your account is deleted, whichever comes first. Additionally, we may retain information from deleted accounts to comply with the law, prevent fraud, resolve disputes, troubleshoot problems, assist with investigations, enforce the Terms of Use, and take other actions permitted by law. The information we retain will be handled in accordance with this Policy.</p>
        <p>Information about you that is no longer necessary and relevant to provide our Services may be de-identified and aggregated with other non-personal data to provide insights which are commercially valuable to The Revolution Token, such as statistics related to the use of The Revolution Token website and application(s).</p>
        <h1 className="text-center pt-5 pb-4 second-section--title">Children</h1>
        <p>The Revolution Token service is not intended for children under the age of 17, and we do not knowingly collect information from children under the age of 17.
        <br/>Children aged 17 or younger should not submit any personal information without the permission of their parents or guardians. By using The Revolution Token service, you are representing that you are at least 18 years old, or that you are at least 17 years old and have your parents’ or guardians’ permission to use the Service.
        </p>
        <h1 className="text-center pt-5 pb-4 second-section--title">Consent to Worldwide Transfer and Processing of Personal Information</h1>
        <p>The Revolution Token is in the United States and the terms of this Policy, and the related Terms of Use shall be governed by and construed in accordance with U.S. federal law and applicable state law, without regard to any principles of conflicts of law. If you are not located in the United States, by accessing the Website and Services and providing personal information through it, you agree and acknowledge and consent to the collection, maintenance, processing, and transfer of such information in and to the United States and other countries and territories. These other jurisdictions may have different privacy laws from your home jurisdiction and provide different levels of protection of personal information. The Revolution Token will implement processes and procedures to provide adequate levels of protection to protect the transfer of data from the European Union. You agree that the terms of this Policy and the Terms of Use will apply, and you consent to the transmission and processing of your personal information in any jurisdiction.</p>
        <h1 className="text-center pt-5 pb-4 second-section--title">EU and EEA Users’ Rights</h1>
        <p>If you are habitually located in the European Union or European Economic Area, you generally have the right to access, rectify, download or erase your information, as well as the right to restrict and object to certain processing of your information. While some of these rights apply generally, certain rights apply only in certain limited circumstances. We briefly describe these rights below:</p>
        <p>You have the right to access your personal data and, if necessary, have it amended, deleted or restricted. In certain instances, you may have the right to the portability of your data. You can also ask us to not send marketing communications and not to use your personal data when we carry out profiling for direct marketing purposes. You can opt out of receiving e-mail newsletters and other marketing communications by following the opt-out instructions provided to you in those e-mails. Transactional account messages will be unaffected even if you opt out from marketing communications.</p>
        <h1 className="text-center pt-5 pb-4 second-section--title">Complaints</h1>
        <p>Should you wish to raise a concern about our use of your information (and without prejudice to any other rights you may have), you have the right to do so with your local supervisory authority, although we hope that we can assist with any queries or concerns you may have about our use of your personal data.</p>
        <h1 className="text-center pt-5 pb-4 second-section--title">Your California Privacy Rights</h1>
        <p>California Consumer Privacy Act (CCPA)</p>
        <p>In the last twelve months, The Revolution Token may have collected, used, and shared, for business purposes, personal information about you as described in this Policy. Each category of data may be used by The Revolution Token or shared with third parties also as described in this Policy. Residents of California have the right to request access to and deletion of the information The Revolution Token holds about them. Such requests may be submitted by email to info@therevolutiontoken.com or by mail to The Revolution Token. The Revolution Token will not sell your personal information without your consent. The Revolution Token will not discriminate against you for exercising your rights under CCPA. Specifically, The Revolution Token will not:</p>
        <ul>
          <li>Deny access to our Services</li>
          <li>Charge a different rate for the use of our Services</li>
          <li>Provide a different quality of service.</li>
        </ul>
        <h1 className="text-center pt-5 pb-4 second-section--title">Changes</h1>
        <p>The Revolution Token may periodically update this policy. We may notify you about significant changes in the way we treat personal information by placing a prominent notice on our Website so please check back occasionally to ensure that you agree with the changes. If you do not, do not use the Website, the application or our services.</p>
        <h1 className="text-center pt-5 pb-4 second-section--title">Questions</h1>
        <p>Any questions about this Policy should be addressed to this e-mail address: info@therevolutiontoken.com.
<br/>Effective Date: April 11, 2022.
</p>
      </div>
    </div>
    </div>
  );
}
export default PrivacyPolicy;
